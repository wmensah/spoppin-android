package net.wilmens.spoppin.objects;

public enum ScoreCategoryEnum {
	Drinks,
	Music,
	Girls,
	Guys;
}
