package net.wilmens.spoppin.objects;

public class VenueScoreInfo {
	private double drinks;
	public double getDrinks(){
		return drinks;
	}
	public void setDrinks(double value){
		drinks = value;
	}
	
	private double music;
	public double getMusic(){
		return music;
	}
	public void setMusic(double value){
		music = value;
	}
	
	private double girls;
	public double getGirls(){
		return girls;
	}
	public void setGirls(double value){
		girls = value;
	}
	
	private double guys;
	public double getGuys(){
		return guys;
	}
	public void setGuys(double value){
		guys = value;
	}
}
