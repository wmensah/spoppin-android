package net.wilmens.spoppin;

import net.wilmens.spoppin.R;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MenuListFragment extends ListFragment{
	
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list, null);
    }

	public void onActivityCreated(Bundle savedInstanceState) {
	        super.onActivityCreated(savedInstanceState);
	        SampleAdapter adapter = new SampleAdapter(getActivity());
	        
	        adapter.add(new SampleItem("Map", R.drawable.ic_action_place));
	        adapter.add(new SampleItem("Settings", R.drawable.ic_action_settings));
	        
	        setListAdapter(adapter);
	}
	
	@Override
    public void onListItemClick(ListView l, View v, int position, long id) {
		
		Intent i = null;
        switch(position){
        	case 0:
        		//Map
        		i = new Intent(this.getActivity().getApplicationContext(), VenueMapActivity.class);
        		this.startActivity(i);
        		break;
        	case 1:
        		//Settings
        		i = new Intent(this.getActivity().getApplicationContext(), SettingsActivity.class);
        		this.startActivity(i);
        		break;
        }
    }

	private class SampleItem {
	        public String tag;
	        public int iconRes;
	        public SampleItem(String tag, int iconRes) {
	                this.tag = tag; 
	                this.iconRes = iconRes;
	        }
	}

	public class SampleAdapter extends ArrayAdapter<SampleItem> {
	
	        public SampleAdapter(Context context) {
	                super(context, 0);
	        }
	
	        public View getView(int position, View convertView, ViewGroup parent) {
	                if (convertView == null) {
	                        convertView = LayoutInflater.from(getContext()).inflate(R.layout.row, null);
	                }
	                ImageView icon = (ImageView) convertView.findViewById(R.id.row_icon);
	                icon.setImageResource(getItem(position).iconRes);
	                TextView title = (TextView) convertView.findViewById(R.id.row_title);
	                title.setText(getItem(position).tag);
	
	                return convertView;
	        }
	
	}
}
